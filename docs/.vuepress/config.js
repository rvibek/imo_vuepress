module.exports = {
    title: 'EGYPT InfoHub',
    description: 'IM Datasets: Egypt running on GitLab Pages',
    base: '/imo_vuepress/',
    dest: 'public',


    themeConfig: {
	    nav: [
	      { text: 'Home', link: '/' },
	      { text: 'Guide', link: '/guide/' },
	      { text: 'Contact Us', link: '/contact/' },
	    ],

	    sidebar:  {

	    	'/guide/': [
	    	'',
	    	'administrative-maps',
	    	'dataportal',
	    	'data-repository',
	    	'activityinfo'

	    	],

	    '/': [
	        '', 
	        ['/guide/', 'Guide'],      /* / */
	        ['/contact/', 'Contact Us'], /* /contact.html */
	
	      ]
	    }
  }
}

