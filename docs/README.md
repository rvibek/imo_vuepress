---
home: true
heroImage: /hero_ed.png
heroText: " "
tagline: Egypt Information Hub
actionText: Get Started →
actionLink: /guide/
features:
- title: Simplicity 
  details: Minimal setup with markdown-centered project structure helps you focus on writing.
- title: Vuepress
  details: Enjoy the dev experience of Vue + webpack, use Vue components in markdown, and develop custom themes with Vue.
- title: Performant
  details: VuePress generates pre-rendered static HTML for each page, and runs as an SPA once a page is loaded.
footer: MIT Licensed | Copyright © 2019 Egypt IMWG
---


::: tip
Lorem ipsum dolor sit amet
:::
