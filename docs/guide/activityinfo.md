# ActivityInfo

[ActivityInfo](http://activityinfo.org) is an online system that allows users to store, share and analyze data. Using ActivityInfo, we aim to harmonize reporting and data collection, since using excel sheets is extremely time-consuming and vulnerable to errors such as duplication or missed values. Organizations report to all sectors by submitting data once a month on the 10th of the month through ActivityInfo.

## Access ActivityInfo

* All users need to sign up for a free ActivityInfo account, preferably with a work email account. Sign up here: [https://www.activityinfo.org/signUp](https://www.activityinfo.org/signUp)
* To gain access to any database, please contact your sector coordinator and sectoral IM.

## Overview of ActivityInfo
There are four main sections of ActivityInfo:

1. **Dashboard:** Your home page, displays reports you’ve saved.

2. **Data Entry:** Where you’ll enter data to report.

3. **Reports:** For generating charts, pivot tables, and maps.

4. **Design:** For designing database and changing administration settings.

**ActivityInfo.org has user guides online at the links below:**

* [Pivot table](http://about.activityinfo.org/learn/reference/analyzing-results/creating-pivot-tables/)
* [Charts](http://about.activityinfo.org/learn/reference/analyzing-results/charting-data/)



## Sector Focal Points
ActvityInfo database is based on the sector monitoring and evaluation frameworks prepared for each sector individually by the relevant sector coordinator. 

| Sector  | Focal Person  | Email   | 
|---|:-:|---|
|  Protection |   |   |
| Basic Needs & Livelihood  |   |   |
|  Education |   |   |
|  Health |   |   |
|  Food Security |   |   |

